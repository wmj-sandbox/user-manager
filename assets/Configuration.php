<?php

namespace Wmj\UserManager\Assets;

class Configuration
{
    function getDbConnectString():string{
        return 'mysql:host=mysql;port=3306;dbname=usermanager';
    }

    function getDbUser():string{
        return 'usermanager';
    }

    function getDbPassword():string{
        return 'pwd';
    }

    function getApiKey():string{
        return '';
    }

    function getPepper():string{
        return '';
    }
}