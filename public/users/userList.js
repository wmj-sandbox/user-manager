const UserList = {
    template: `
        <div class="d-flex justify-content-center">
        <main class="card w-50 card-shadow" style="box-sizing: border-box">
            <div class="card-header position-sticky sticky-top bg-light">
                <h5 class="card-title">User list</h5>
                <div class="p1">
                    <button type="button" class="btn btn-outline-warning btn-sm rounded-corners" @click="refreshUsers">Reload </button>
                    <button type="button" @click="addUser()" class="btn btn-outline-primary btn-sm rounded-corners">Add</button>
                </div>
            </div>
            <div class="card-body">
<!--                <div class="w-100" style="margin-bottom: 2vmin;">-->
<!--                    <input id="userListSearch" v-model="searchedUserName" @change="matchUserName()" type="text" class="form-control rounded-3" placeholder="Filter users"/>-->
<!--                </div>-->
                <button class="placeholder form-control btn-light" v-if="isUserListEmpty" disabled>No users</button>
                <div v-if="!isUserListEmpty">
                    <button type="button" @click="userDetails(user.index)" class="btn btn-sm form-control btn-outline-secondary rounded-corners m-1" v-for="user in userList" :key="user.index" v-if="user.name">#{{ user.index }}: {{ user.name }}</button>
                </div>
            </div>
        </main>
        </div>
    `,
    data() {
        return {
            userList: [],
        }
    },
    computed:{
      isUserListEmpty(){
          return this.usersToDisplay.length === 0
      },
        usersToDisplay(){
          return this.userList
        }
    },
    methods: {
        refreshUsers(){
            $.get({
                url: '/api/v1/users',
                success: data => this.userList = data,
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })

        },
        addUser(){
            this.$router.push('/user-add')
        },
        userDetails(id){
            this.$router.push(`/user-details/${id}`)
        },
    },
    mounted(){
        this.refreshUsers()
    }
}