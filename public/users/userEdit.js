const UserEdit = {
    template: `
        <div class="d-flex justify-content-center">
        <main class="card w-50 card-shadow" style="box-sizing: border-box">
            <div class="card-header position-sticky sticky-top bg-light">
                <h5 class="card-title">Edit user #{{ userIndex }}</h5>
                <div class="p-1">
                    <button type="button" @click="backToDetails" class="btn btn-outline-secondary btn-sm rounded-corners">Back to details</button>

                </div>
            </div>
            <div class="card-body">
            <form action="" @keyup.enter.prevent="updateUser">
                <table class="table" id="newUserForm">
                    <tbody>
                        <tr>
                            <td><input type="checkbox" v-model="include.name" ref="formStart"></td>
                            <td style="text-align: right">Name:</td>
                            <td><input v-model="userDetails.name" type="text" class="form-control form-check-input rounded-corners w-100" :disabled="!include.name"></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" v-model="include.password"></td>
                            <td style="text-align: right">Password:</td>
                            <td><input v-model="userDetails.password" type="password" class="form-control form-check-input rounded-corners w-100" :disabled="!include.password" ></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" v-model="include.firstname"></td>
                            <td style="text-align: right">First name:</td>
                            <td><input v-model="userDetails.firstname" type="text" class="form-control form-check-input rounded-corners w-100" :disabled="!include.firstname"></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" v-model="include.lastname"></td>
                            <td style="text-align: right">Last name:</td>
                            <td><input v-model="userDetails.lastname" type="text" class="form-control form-check-input rounded-corners w-100"  :disabled="!include.lastname"></td>
                        </tr>
                        <tr >
                            <td><input type="checkbox" v-model="include.birthday"></td>
                            <td style="text-align: right">Birth day:</td>
                            <td><input v-model="userDetails.birthday" type="date" class="form-control form-check-input rounded-corners w-100"  :disabled="!include.birthday"></td>
                        </tr>
                    </tbody>
                </table>
            </form>
            </div>
            <div class="card-footer d-flex flex-row-reverse">
                <button href="#" type="submit" form="newUserForm" @click="updateUser" class="btn btn-success  rounded-corners" :disabled="enableUpdateButton">Update</button>
            </div>
        </main>

    `,
    data() {
        return {
            userDetails: {
                index: 1,
                password: null,
                name: null,
                firstname: null,
                lastname: null,
                birthday: null
            },
            errorMessage: null,
            include:{
                name: false,
                password: false,
                firstName: false,
                lastName: false,
                birthday: false
            }
        }

    },
    computed:{
        userIndex(){
            return this.$route.params.index
        },
        enableUpdateButton(){
            return  !Object.values(this.include).includes(true)
        }
    },
    methods: {
        backToDetails(){
            this.$router.push(`/user-details/${this.userIndex}`)
        },
        updateUser() {
            const payload = {
                index: this.userIndex
            }
            if (this.include.name) payload.name = this.userDetails.name
            if (this.include.password) payload.password = this.userDetails.password
            if (this.include.firstname) payload['first_name'] = this.userDetails.firstname
            if (this.include.lastname) payload['last_name'] = this.userDetails.lastname
            if (this.include.birthday) payload.birthday = this.userDetails.birthday
            console.log(payload)

            $.ajax({
                method: 'POST',
                url: `/api/v1/users/${payload.index}`,
                cache: false,
                data: payload,
                success: result => {
                    this.$router.push(`/user-details/${this.userIndex}`)
                },
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        }
    },
    mounted(){
        this.$refs.formStart.focus()
    }
}