const UserCreate = {
    template: `
        <div class="d-flex justify-content-center"  >
        <main class="card w-50 card-shadow " style="box-sizing: border-box">
            <div class="card-header position-sticky sticky-top bg-light">
                <h5 class="card-title">User create</h5>
                <div class="">
                    <button type="button" @click="backToList" class="rounded-corners btn-sm btn btn-outline-secondary ">Back to list</button>

                </div>
            </div>
            <div class="card-body">
            <form action="" @keyup.enter.prevent="createUser">
                <table class="table" id="newUserForm">
                    <tbody>
                        <tr>
                            <td><input type="checkbox" v-model="include.name" disabled></td>
                            <td style="text-align: right">* Name:</td>
                            <td><input v-model="userDetails.name" type="text" class="form-control form-check-input w-100 rounded-corners" ref="formStart" required ></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" v-model="include.password" disabled></td>
                            <td style="text-align: right">* Password:</td>
                            <td><input v-model="userDetails.password" type="password" class="form-control form-check-input w-100 rounded-corners"  required ></td>
                        </tr>
                        <tr class="bg-light">
                            <td><input type="checkbox" v-model="include.firstName"></td>
                            <td style="text-align: right">First name:</td>
                            <td><input v-model="userDetails.firstName" type="text" class="form-control form-check-input w-100 rounded-corners""></td>
                        </tr>
                        <tr class="bg-light">
                            <td><input type="checkbox" v-model="include.lastName"></td>
                            <td style="text-align: right">Last name:</td>
                            <td><input v-model="userDetails.lastName" type="text" class="form-control form-check-input w-100 rounded-corners" ></td>
                        </tr>
                        <tr class="bg-light">
                            <td><input type="checkbox" v-model="include.birthday"></td>
                            <td style="text-align: right">Birth day:</td>
                            <td><input v-model="userDetails.birthday" type="date" class="form-control form-check-input w-100 rounded-corners" ></td>
                        </tr>
                    </tbody>
                </table>
            </form>
            </div>
            <div class="card-footer d-flex flex-row-reverse">
                <button href="#" type="submit" form="newUserForm" @click="createUser" class="rounded-corners btn btn-outline-success " >Create</input>
            </div>
        </main>
    `,
    data() {
        return {
            userDetails: {
                name: null,
                password: null,
                firstName: null,
                lastName: null,
                birthday: null
            },
            errorMessage: null,
            include: {
                name: true,
                password: true,
                firstName: false,
                lastName: false,
                birthday: false
            }
        }
    },
    methods: {
        backToList() {
            this.$router.push('user-list')
        },
        createUser() {
            const payload = {
                name: this.userDetails.name,
                password: this.userDetails.password
            }
            if (this.include.firstName) payload['first_name'] = this.userDetails.firstName
            if (this.include.lastName) payload['last_name'] = this.userDetails.lastName
            if (this.include.birthday) payload.birthday = this.userDetails.birthday

            console.log(payload)
            console.log(this.include)
            $.ajax({
                method: 'POST',
                url: '/api/v1/users',
                data: payload,
                success: (message) => this.$router.push('/user-list'),
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        }
    },
    mounted() {
        this.$refs.formStart.focus()
    }
}