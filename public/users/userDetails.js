const UserDetails = {
    template: `
        <div class="d-flex justify-content-center">
        <main class="card w-50 card-shadow" style="box-sizing: border-box">
            <div class="card-header position-sticky sticky-top bg-light">
                <h5 class="card-title">User details</h5>
                <nav class="">
                    <button type="button" class="btn btn-outline-secondary btn-sm rounded-corners" @click="backToList()">Back to list </button>
                    <button type="button" @click="editUser" class="btn btn-outline-primary btn-sm rounded-corners">Edit user</button>
<!--                    <button type="button" @click="editGroups" class="btn btn-primary rounded-3">Edit groups</button>-->
                </nav>
            </div>
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr >
                            <td style="text-align: right">#:</td>
                            <td>{{ userIndex }}</td>
                        </tr>
                        <tr >
                            <td style="text-align: right">User name:</td>
                            <td>{{  userDetails.name }}</td>
                        </tr>
                        <tr >
                            <td style="text-align: right">FirstName:</td>
                            <td>{{ userDetails.firstname }}</td>
                        </tr>
                        <tr >
                            <td style="text-align: right">Last Name:</td>
                            <td>{{  userDetails.lastname }}</td>
                        </tr>
                        <tr >
                            <td style="text-align: right">Birthday:</td>
                            <td>{{  userDetails.birthday }}</td>
                        </tr>
                        <tr >
                            <td style="text-align: right">Groups:</td>
                            <td class="d-flex">
         
                                <button class="btn bg-info text-light rounded-corners" style="margin-left: 1vmin; padding: 4px; box-sizing: border-box" @click="goToGroup(group.index)" v-for="(group,id) in allGroups" :key="id" v-if="whetherInGroup(group.index)">{{ group.name }}</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer d-flex flex-row-reverse">
                <button type="button" class="btn btn-outline-danger rounded-corners" @click="deleteUser()">Delete </button>
            </div>
        </main>
        </div>
    `,
    data() {
        return {
            userDetails: {
                name: '',
                firstname: '',
                lastname: '',
                birthday: null
            },
            groups: [
                {index:1, name:'Group1'}
            ],
            allGroups: [],
            inGroup: []
        }

    },
    computed:{
        userIndex(){
            return this.$route.params.index
        },

    },
    methods: {
        whetherInGroup(index){
            // console.log()
            return this.inGroup.includes(index)
        },
        changeData(data){
            this.userDetails.name = data.name
            this.userDetails.firstname = data['first_name']
            this.userDetails.lastname = data['last_name']
            this.userDetails.birthday = data.birthday
        },
        refreshUser(){
            console.log('refresh')
            $.ajax({
                method: 'GET',
                url: `/api/v1/users/${this.userIndex}`,
                dataType: 'json',
                success: data => this.changeData(data),
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
            //todo: download user groups from users/${this.userIndex}/groups GET
        },
        deleteUser(){
            // call DELETE /users/:index
            // after emit toast and success redirect to list
            $.ajax({
                method: 'DELETE',
                cache: false,
                url: `/api/v1/users/${this.userIndex}`,
                success: () => {
                    this.$router.push('/user-list')
                },
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        },
        editUser(){
            this.$router.push(`/user-edit/${this.userIndex}`)
        },
        editGroups(){
            this.$router.push(`/user-groups-edit/${this.userIndex}`)
        },
        backToList(){
            this.$router.push('/user-list')
        },
        downloadGroups(){
            $.ajax({
                method: 'GET',
                url: "/api/v1/users-groups",
                success: users => {
                    this.allGroups = users
                } ,
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        },
        downloadInGroups(){
            $.ajax({
                method: 'GET',
                url: `/api/v1/users/${this.userIndex}/groups`,
                success: groups => {
                    this.inGroup = this.converGroupsToIndexList(groups)
                    this.activeMembers = this.members
                },
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        },
        converGroupsToIndexList(list){
            const result = [];
            list.forEach(group => result.push(group['group_index']))
            return result
        },
        goToGroup(index){
            this.$router.push(`/group-details/${index}`)
        }
    },
    mounted(){
        this.refreshUser()
        this.downloadGroups()
        this.downloadInGroups()
    }
}