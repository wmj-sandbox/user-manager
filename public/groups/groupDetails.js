const GroupDetails = {
    template: `
        <div class="d-flex justify-content-center">
        <main class="card w-50 card-shadow" style="box-sizing: border-box">
            <div class="card-header position-sticky sticky-top bg-light">
                <h5 class="card-title">Group details</h5>
                <div class="p-2 ">
                    <button type="button" class="btn btn-sm btn-outline-secondary rounded-corners" @click="backToList">Back to list </button>

                    <button type="button" @click="editGroup" class="btn btn-sm btn-outline-primary rounded-corners">Edit group</button>
                    <button type="button" @click="editMembers" class="btn btn-sm btn-outline-primary rounded-corners">Edit members</button>
                </div>
            </div>
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr >
                            <td style="text-align: right">#:</td>
                            <td>{{ groupDetails.index }}</td>
                        </tr>
                        <tr >
                            <td style="text-align: right">Name:</td>
                            <td>{{ groupDetails.name }}</td>
                        </tr>
                        <tr>
                            <td style="text-align: right">Members count:</td>
                            <td>
                                <span class="bg-light rounded-3 p-1 text-dark">{{ membersCount }}</span>
                            </td>
<!--                            <td class="d-flex">-->
<!--                                <div class="bg-light text-dark rounded-3" style="margin-left: 1vmin; padding: 4px; box-sizing: border-box" v-for="user in members" :key="user['user_index']">#{{ user['user_index'] }}</div>-->
<!--                            </td>-->
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer d-flex flex-row-reverse">
                <button type="button" class="btn btn-outline-danger rounded-corners" @click="deleteGroup">Delete </button>
            </div>
        </main>
        </div>
    `,
    data() {
        return {
            groupDetails: {
                index: null,
                name: null
            },
            members: [
                {index:1, name:'Group1'}
            ]
        }
    },
    computed:{
        getIndex(){
            return this.$route.params.index
        },
        membersCount(){
            return this.members.length
        }
    },
    methods: {
        refreshGroup(){
            $.ajax({
                method: 'GET',
                url: `/api/v1/users-groups/${this.getIndex}`,
                success: (data) => {
                    this.groupDetails = data
                    this.refreshMembers()
                },
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        },
        refreshMembers(){
            $.ajax({
                method: 'GET',
                url: `/api/v1/users-groups/${this.getIndex}/members`,
                success: (data) =>{
                    console.log(data)
                    this.members = data
                },
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        },
        deleteGroup(){
            $.ajax({
                method: 'DELETE',
                url: `/api/v1/users-groups/${this.getIndex}`,
                success: (data) => this.$router.push('/group-list'),
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        },
        editGroup(){
            this.$router.push(`/group-edit/${this.getIndex}`)
        },
        editMembers(){
            this.$router.push(`/group-members-edit/${this.getIndex}`)
        },
        backToList(){
            this.$router.push('/group-list')
        }
    },
    mounted(){
        this.refreshGroup()
    }
}