const GroupList = {
    template: `
        <div class="d-flex justify-content-center">
        <main class="card w-50 card-shadow" style="box-sizing: border-box">
            <div class="card-header position-sticky sticky-top bg-light">
                <h5 class="card-title">Group list</h5>
                <div class="p-1">
                    <button type="button" class="btn btn-sm btn-outline-warning rounded-corners" @click="refreshGroups">Refresh </button>
                    <button type="button" @click="addGroup()" class="btn btn-sm btn-outline-primary rounded-corners">Add</button>
                </div>
            </div>
            <div class="card-body">
<!--                <div class="w-100" style="margin-bottom: 2vmin;">-->
<!--                    <input id="userListSearch" v-model="searchedUserName" @change="matchUserName()" type="text" class="form-control rounded-3" placeholder="Filter users"/>-->
<!--                </div>-->
                <button class="placeholder form-control btn-light" v-if="isGroupListEmpty" disabled>No groups</button>
                <div v-if="!isGroupListEmpty">
                    <button type="button" @click="groupDetails(group.index)" class="btn btn-sm form-control btn-outline-secondary rounded-corners m-1" v-for="group in groupList" :key="group.index" v-if="group.name">#{{ group.index }}: {{ group.name }}</button>
                </div>
             
            </div>
            <div class="card-footer"></div>
        </main>
        </div>
    `,
    data() {
        return {
            groupList: [],
        }
    },
    computed:{
      isGroupListEmpty(){
          return this.usersToDisplay.length === 0
      },
        usersToDisplay(){
          return this.groupList
        }
    },
    methods: {
        refreshGroups(){
            $.get({
                url: '/api/v1/users-groups',
                dataType: 'json',
                success: data => {
                    this.groupList = data
                    if (data.length === 0) alert('No group')
                },
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }

            })
        },
        addGroup(){
            this.$router.push('/group-create')
        },
        groupDetails(id){
            this.$router.push(`/group-details/${id}`)
        }
    },
    mounted(){
        this.refreshGroups()
    }
}