const GroupEdit = {
    template: `
        <div class="d-flex justify-content-center">
        <main class="card w-50 card-shadow" style="box-sizing: border-box">
            <div class="card-header position-sticky sticky-top bg-light">
                <h5 class="card-title">Edit group {{ getIndex }}</h5>
                <div class="p-1">
                    <button type="button" @click="backToDetails" class="btn btn-outline-secondary btn-sm rounded-corners">Back to details</button>

                </div>
            </div>
            <div class="card-body">
            <form action="" @keyup.enter.prevent="updateGroup">
                <table class="table" id="newUserForm">
                    <tbody>
                        <tr>
                            <td><input type="checkbox" v-model="include.name" ref="formStart"></td>
                            <td style="text-align: right">Name:</td>
                            <td><input v-model="groupDetails.name" type="text" class="form-control form-check-input w-100" :disabled="!include.name"></td>
                        </tr>
                    </tbody>
                </table>
            </form>
            </div>
            <div class="card-footer">
                {{ errorMessage }}
            </div>
            <div class="card-footer d-flex flex-row-reverse">
               <button href="#" type="submit" form="newUserForm" @click="updateGroup" class="btn btn-outline-success  rounded-corners" :disabled="enableUpdateButton">Update</buttton>
            </div>
        </main>

    `,
    data() {
        return {
            groupDetails: {
                index: null,
                name: null
            },
            errorMessage: null,
            include:{
                name: false
            }
        }

    },
    computed:{
        getIndex(){
            return this.$route.params.index
        },
        enableUpdateButton(){
            return  !Object.values(this.include).includes(true)
        }
    },
    methods: {
        backToDetails(){
            this.$router.push(`/group-details/${this.getIndex}`)
        },
        updateGroup() {
            const payload = {
                index: this.getIndex
            }
            if (this.include.name) payload.name = this.groupDetails.name

            console.log(payload)

            $.ajax({
                method: 'POST',
                url: `/api/v1/users-groups/${payload.index}`,
                cache: false,
                data: payload,
                success: () => this.$router.push(`/group-details/${this.getIndex}`),
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        }
    },
    mounted(){
        this.$refs.formStart.focus()
    }
}