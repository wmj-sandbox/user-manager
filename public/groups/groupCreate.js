const GroupCreate = {
    template: `
        <div class="d-flex justify-content-center">
        <main class="card w-50 card-shadow" style="box-sizing: border-box">
            <div class="card-header position-sticky sticky-top bg-light">
                <h5 class="card-title">Group create</h5>
                <div class="p-1">
                    <button type="button" @click="backToList" class="btn btn-sm btn-outline-secondary rounded-corners">Back to list</button>

                </div>
            </div>
            <div class="card-body">
            <form action="" @keyup.enter.prevent="createGroup">
                <table class="table" id="newUserForm">
                    <tbody>
                        <tr >
                            <td style="text-align: right">* Name:</td>
                            <td><input v-model="groupDetails.name" type="text" class="form-control form-check-input w-100 rounded-corners"  ref="formStart" required ></td>
                        </tr>
                    </tbody>
                </table>
            </form>
            </div>
            <div class="card-footer d-flex flex-row-reverse">
                <button href="#" type="submit" form="newUserForm" @click="createGroup" class="btn btn-outline-success rounded-corners ">Create</input>
            </div>
        </main>

    `,
    data() {
        return {
            groupDetails: {
                name: null,
            },
            errorMessage: null
        }

    },
    methods: {
        backToList(){
            this.$router.push('/group-list')
        },
        createGroup(){
            const payload = this.groupDetails
            console.log(payload)

            $.ajax({
                method: 'POST',
                url: '/api/v1/users-groups',
                data: payload,
                success: (message) => this.$router.push('/group-list'),
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        }
    },
    mounted(){
        this.$refs.formStart.focus();
    }
}