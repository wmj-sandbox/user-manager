const GroupMembersEdit = {
    template: `
        <div class="d-flex justify-content-center"  ref="componentBody">
        <main class="card w-50 card-shadow" style="box-sizing: border-box">
            <div class="card-header position-sticky sticky-top bg-light">
                <h5 class="card-title">Edit group {{ userIndex }} members.</h5>
                <div class="p-1">
                    <button type="button" @click="backToDetails"  class="btn btn-outline-secondary btn-sm rounded-corners">Back to details</button>

                </div>
            </div>
            <div class="card-body">
            <form action="" @keyup.enter.prevent="updateMembers">
                <table class="table" id="newUserForm">
                    <thead><tr><td colspan="2">Contains<td></td></tr></thead>
                    <tbody>
                        <tr v-for="(member, index) in availableUsers" :key="index" v-if="inMembers(member.index)">
                            <td style="text-align: right"><input type="checkbox"  :value="member.index" v-model="activeMembers" @click="enableSubmit"></td>
                            <td>#{{ member.index }}</td>
                            <td>{{ member.name }}</td>
                        </tr>
                    </tbody>
                </table>
                <table class="table" id="newUserForm">
                    <thead><tr><td colspan="2">Available users <td></td></tr></thead>
                    <tbody>
                        <tr v-for="(member, index) in availableUsers" :key="index" v-if="!inMembers(member.index)" >
                            <td style="text-align: right"><input type="checkbox" :value="member.index" v-model="activeMembers" @click="enableSubmit"></td>
                            <td>#{{ member.index }}</td>
                            <td>{{ member.name }}</td>
                        </tr>
                    </tbody>
                </table>
            </form>
            </div>
            <div class="card-footer d-flex flex-row-reverse">
                                    <button href="#" type="submit"  @click="updateMembers" class="btn btn-outline-success  rounded-corners" :disabled="submitDisabled" >Update</input>
            </div>
        </main>

    `,
    data() {
        return {
            userDetails: {
                index: 1,
                name: null
            },
            errorMessage: null,
            members:[
                {
                    user_index: null,
                    name: null
                }
            ],
            allUsers:[

            ],
            activeMembers: [],
            submitDisabled: true
        }

    },
    props:{
        userIndex: {
            default: null
        },
        userName: {
            default: null
        }
    },
    computed:{
        groupIndex(){
            return this.$route.params.index
        },
        availableUsers(){

            // console.log(this.getObjectDiff(this.allGroups, this.memberOf))
            return this.getObjectDiff(this.allUsers, this.members)
        }
    },
    methods: {
        enableSubmit(){
            this.submitDisabled = false
        },
        downloadGroupMembers(){
            // get from api
            $.ajax({
                method: 'GET',
                url: `/api/v1/users-groups/${this.groupIndex}/members`,
                success: users => {
                    this.members = this.converUserToIndexList(users)
                    this.activeMembers = this.members
                },
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        },
        downloadUsers(){
            $.ajax({
                method: 'GET',
                url: "/api/v1/users",
                success: users => {
                    console.log(users)
                    this.allUsers = users
                } ,
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        },
        inMembers(index){
            return this.members.includes(index)
        },
        converUserToIndexList(list){
            const result = [];
            list.forEach(group => result.push(group['user_index']))
            return result
        },
        getObjectDiff(obj1, obj2){
            const result = {};
            $.each(obj1, function (key, value) {
                if (!obj2.hasOwnProperty(key) || obj2[key] !== obj1[key]) {
                    result[key] = value;
                }
            });
            return result;
        },
        backToDetails() {
            this.$router.push(`/group-details/${this.groupIndex}`)
        },
        updateMembers() {
            console.log(this.activeMembers)
            const payload = {
                members: this.activeMembers
            }
            $.ajax({
                method: 'POST',
                url: `/api/v1/users-groups/${this.groupIndex}/members`,
                cache: false,
                data: payload,
                success: () => this.$router.push(`/group-details/${this.groupIndex}`),
                error: (error) => {
                    console.log(error)
                    alert('Error occurred.')
                }
            })
        }
    },
    mounted(){
        this.downloadUsers()
        this.downloadGroupMembers()

        window.addEventListener('keyup', event => {
            if(event.keyCode === 8 ){
                this.backToDetails()
            }
        })

    }
}