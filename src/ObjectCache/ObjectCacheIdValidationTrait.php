<?php


namespace Wmj\UserManager\ObjectCache;


use InvalidArgumentException;

trait ObjectCacheIdValidationTrait
{
    /**
     * @param $id
     */
    protected function validateIdOrFail($id): void
    {
        switch (true) {
            case is_int($id) && $id > 0:
            case is_string($id) && $id !== '':
            case is_null($id):
                return;
            default:
                throw new InvalidArgumentException(
                    implode(' ', ['Cache id must me integer or string', gettype($id), 'given'])
                );
        }
    }
}