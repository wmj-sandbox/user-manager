<?php


namespace Wmj\UserManager\ObjectCache;


use WeakReference;

/**
 * Trait ObjectCacheTrait
 *
 * @package Wmj\UserManager
 */
trait ObjectCacheTrait
{
    use ObjectCacheIdValidationTrait;

    /**
     * @var WeakReference[]
     */
    protected array $cache = [];

    /**
     * @param object $subject
     * @param null   $id
     */
    function addToCache(object $subject, $id = null): object
    {
        $this->validateIdOrFail($id);
        $this->cache[$id ?? get_class($subject)] = $subject;

        return $subject; //todo: add test case that retrurn same $subject as input
    }

    /**
     * @param $id
     *
     * @return object
     */
    function getFromCache($id): ?object
    {
        return $this->cache[$id] ?? null;
    }
}