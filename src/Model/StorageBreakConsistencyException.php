<?php


namespace Wmj\UserManager\Model;


use Throwable;

class StorageBreakConsistencyException extends DomainException
{
    function __construct(string $entryClass, EntryIndex $index, $message = "", $code = 0, Throwable $previous = null)
    {
        $message = "Repository $entryClass has duplicated index $index." . ' ' . $message;
        parent::__construct($message, $code, $previous);
    }
}