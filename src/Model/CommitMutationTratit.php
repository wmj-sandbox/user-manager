<?php


namespace Wmj\UserManager\Model;


trait CommitMutationTratit
{
    private array $members = [];
    private array $pendingAdditions = [];
    private array $pendingRemoves = [];

    function hasPendingChanges(): bool
    {
        return $this->hasPendingRemoves() || $this->hasPendingAdditions();
    }

    function hasPendingRemoves(): bool
    {
        return !empty($this->pendingRemoves);
    }

    function hasPendingAdditions(): bool
    {
        return !empty($this->pendingAdditions);
    }

    function getPendingAdditions(): array
    {
        return $this->pendingAdditions;
    }

    function getPendingRemoves(): array
    {
        return $this->pendingRemoves;
    }

    function commitChanges(): void
    {
        $this->commitRemoves();
        $this->commitAdditions();
    }

    protected function commitRemoves(): void
    {
        $this->members = array_diff_key($this->members, array_keys($this->pendingRemoves));
        $this->clearPendingRemoves();
    }

    private function clearPendingRemoves(): void
    {
        $this->pendingRemoves = [];
    }

    protected function commitAdditions(): void
    {
        $this->members =  array_merge($this->members, $this->pendingAdditions);
        $this->clearPendingAdditions();
    }

    private function clearPendingAdditions(): void
    {
        $this->pendingAdditions = [];
    }

    function rejectChanges(): void
    {
        $this->clearPendingAdditions();
        $this->clearPendingRemoves();
    }

    protected function scheduleToAdd($value, $offset = null): void
    {
        //todo: if member[$offset] not exist cause warning. handle it.

        is_null($offset)
            ? $this->pendingAdditions[] = $value
            : $this->pendingAdditions[$offset] = $value;

//        if(@$this->members[$offset] !== $value){
//            $this->pendingAdditions[$offset] = $value;
//        }
    }

    protected function scheduleToRemove(string $offset): void
    {
        $this->pendingRemoves[$offset] = null;
//        array_push($this->pendingRemoves, $offset);
    }
}