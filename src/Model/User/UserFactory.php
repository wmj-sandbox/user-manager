<?php


namespace Wmj\UserManager\Model\User;


use PDO;
use Wmj\UserManager\Model\EntryIndexCache;
use Wmj\UserManager\Model\IndexExistCheckVisitor;
use Wmj\UserManager\Model\StorageService;
use Wmj\UserManager\ObjectCache\ObjectWeakCacheTrait;


/**
 * Class UserFactory
 *
 * @package Wmj\UserManager\Model\User
 */
class UserFactory
{
    use ObjectWeakCacheTrait;

    protected IndexExistCheckVisitor $indexChecker;
    protected PDO $pdo;

    /**
     * @var StorageService
     */
    protected StorageService $storageService;

    /**
     * UserFactory constructor.
     *
     * @param PDO            $PDO
     * @param StorageService $storageService
     */
    function __construct(PDO $PDO, StorageService $storageService)
    {
        $this->pdo = $PDO;
        $this->storageService = $storageService;

        $repo = $this->getUserRepository();
        $this->getIndexChecker()->setIndexCheckerCallback(fn(UserIndex $index): bool => $repo->userExist($index));
    }

    /**
     * @return UserRepository
     *
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    function getUserRepository(): UserRepository
    {
        return $this->getFromCache(UserRepository::class) ?? $this->addToCache(
                new UserRepository($this->storageService, $this->getMysqlStorage(), $this->makeUserBuilder())
            );
    }

    /**
     * @return MysqlUserStatementBuilder
     *
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    protected function getMysqlStorage(): MysqlUserStatementBuilder
    {
        return $this->getFromCache(MysqlUserStatementBuilder::class) ?? $this->addToCache(
                new MysqlUserStatementBuilder($this->pdo)
            );
    }

    /**
     * @return UserBuilder
     */
    function makeUserBuilder(): UserBuilder
    {
        return new UserBuilder($this->getUserIndexPrototype());
    }

    /**
     * @return UserIndex
     *
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    protected function getUserIndexPrototype(): UserIndex
    {
        return $this->getFromCache(UserIndex::class) ?? $this->addToCache(
                (new UserIndex($this->getIndexChecker(), new EntryIndexCache()))->markAsPrototype()
            );
    }

    /**
     * @return IndexExistCheckVisitor
     *
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    protected function getIndexChecker(): IndexExistCheckVisitor
    {
        return $this->getFromCache(IndexExistCheckVisitor::class) ?? $this->addToCache(new IndexExistCheckVisitor());
    }
}