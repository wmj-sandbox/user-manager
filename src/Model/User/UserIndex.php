<?php


namespace Wmj\UserManager\Model\User;


use Wmj\UserManager\Model\EntryIndex;

/**
 * Class UserIndex
 *
 * @package Wmj\UserManager\Model\User
 */
class UserIndex extends EntryIndex
{
    /**
     * @inheritDoc
     */
    function getEntryClass(): string
    {
        return User::class;
    }
}