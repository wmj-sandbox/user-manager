<?php


namespace Wmj\UserManager\Model\User;


use PDO;
use PDOStatement;
use Wmj\UserManager\Model\ArrayHasDuplicatesTrait;
use Wmj\UserManager\Model\EntryIndex;
use Wmj\UserManager\Model\ReturnPendingAdditionsTrait;
use Wmj\UserManager\Model\StorageService;
use Wmj\UserManager\Model\User\Validation\DuplicatedNameInTransactionGroupException;
use Wmj\UserManager\Model\User\Validation\UserNameAlreadyExistException;

/**
 * Class UserRepository
 *
 * @package Wmj\UserManager\Model\User
 */
class UserRepository
{
    use ReturnPendingAdditionsTrait;
    use ArrayHasDuplicatesTrait;

    const COLLECTION_NAME = 'users';
    const NAME_LABEL = ':name';
    /**
     * @var StorageService
     */
    protected StorageService $storageService;
    /**
     * @var MysqlUserStatementBuilder
     */
    protected MysqlUserStatementBuilder $statementBuilder;
    /**
     * @var UserBuilder
     */
    protected UserBuilder $userBuilder;

    /**
     * UserRepository constructor.
     *
     * @param StorageService            $storageService
     * @param MysqlUserStatementBuilder $userStorage
     * @param UserBuilder               $userBuilder
     */
    function __construct(
        StorageService $storageService,
        MysqlUserStatementBuilder $userStorage,
        UserBuilder $userBuilder
    )
    {
        $this->storageService = $storageService;
        $this->statementBuilder = $userStorage;
        $this->userBuilder = $userBuilder;

        $this->createTable();
    }

    /**
     *
     */
    private function createTable(): void
    {
        $this->storageService->executeStatement($this->statementBuilder->prepareRepositoryCreateStatement());
    }

    /**
     * @param User ...$users
     *
     * @throws DuplicatedNameInTransactionGroupException
     * @throws UserNameAlreadyExistException
     */
    function save(User ...$users): void
    {
        $this->failIfNameDuplicated(... $users);
        $this->storageService->beginTransaction();

        foreach ($users as $user) {
            if ($user->getIndex()->isDefault()) $this->insertUser($user);
            if ($user->hasPendingChanges()) $this->updateUser($user);
        }

        $this->storageService->commitTransaction();
        array_walk($users, fn(User $user) => $user->commitChanges());
    }

    /**
     * @param User ...$users
     *
     * @throws DuplicatedNameInTransactionGroupException
     * @throws UserNameAlreadyExistException
     */
    protected function failIfNameDuplicated(User ...$users): void
    {
        $names = $this->returnPendingAdditions(...$users);
        if ($this->arrayHasDuplicates($names)) throw new DuplicatedNameInTransactionGroupException();
        if ($this->nameExistInDb($names)) throw new UserNameAlreadyExistException();
    }

    /**
     * @param array $names
     *
     * @return bool
     */
    protected function nameExistInDb(array $names): bool
    {
        $statement = $this->statementBuilder->getFindUserWithNameStatement(self::NAME_LABEL);
        foreach ($names as $name) {
            //todo: refactor to one batch select
            $statement->bindValue(self::NAME_LABEL, $name);
            $this->storageService->executeStatement($statement);
            if ($statement->rowCount() > 0) return true;
        }

        return false;
    }

    /**
     * @param User $user
     */
    protected function insertUser(User $user): void
    {
        $statement = $this->statementBuilder->prepareInsertUserStatement();
        $this->storageService->executeStatement($statement);
        $this->userBuilder->changeIndexValue($user, $this->storageService->getLastInsertedId());
    }

    /**
     * @param User $user
     */
    protected function updateUser(User $user): void
    {
        $this->storageService->executeStatement($this->statementBuilder->prepareUpdateUserStatement($user));
    }

    /**
     * @param User ...$users
     */
    function deleteUsers(User ...$users): void
    {
        $this->delete(...array_map(fn(User $user): EntryIndex => $user->getIndex(), $users));
    }

    /**
     * @param UserIndex ...$userIndices
     */
    function delete(UserIndex ...$userIndices): void
    {
        //todo code is redundant in userGroup repository, move its to abstract class or trait
        $this->storageService->beginTransaction();

        $this->storageService->executeStatement(
            $this->statementBuilder->deleteStatement(array_map(fn(UserIndex $index): int => $index->getIndexValue(), $userIndices))
        );

        $this->storageService->commitTransaction();
        array_walk($userIndices, fn(UserIndex $index) => $index->reset());
    }

    /**
     * @param UserIndex $startIndex
     * @param int       $limit
     *
     * @return array
     */
    function loadAllUsers(UserIndex $startIndex, int $limit = -1): array
    {
        return $this->importUsers(
            $this->storageService->executeStatement(
                $this->statementBuilder->prepareGetUserStatement($startIndex, $limit)
            )
        );
    }

    /**
     * @param PDOStatement $statement
     *
     * @return array
     */
    protected function importUsers(PDOStatement $statement): array
    {
        /**
         * @var User[]
         */
        $result = []; //todo: maybe user generators
        $data = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($data as $userData) {
            array_push($result, $this->userBuilder->import($userData));
        }
        return $result;
    }

    /**
     * @param UserIndex $index
     *
     * @return User
     */
    function loadUser(UserIndex $index): User
    {
        return $this->importUsers(
            $this->storageService->executeStatement(
                $this->statementBuilder->selectEntryStmt($index)
            )
        )[0];
    }

    function userExist(UserIndex $index): bool
    {
        return (bool)$this->storageService
            ->executeStatement($this->statementBuilder->entryExistStmt($index))
            ->fetchColumn();
    }

    function loadEntryDetails(UserIndex $index): array
    {
        $statement = $this->statementBuilder->selectEntryStmt($index);
        $this->storageService->executeStatement($statement);
        $userDetails = ($statement->fetch(PDO::FETCH_ASSOC));
        return $userDetails === false ? [] : $userDetails;
    }

    /**
     * @param string $name
     *
     * @return array
     */
    function findUsersWithName(string $name): array
    {
        $statement = $this->statementBuilder->getFindUserWithNameStatement(self::NAME_LABEL);
        $statement->bindValue(self::NAME_LABEL, $name);
        return $this->storageService->executeStatement($statement)->fetch(PDO::FETCH_NUM);
    }

    /**
     * Return all User::index and USER::name
     *
     * @return array
     */
    function loadUserList(): array
    {
        $statement = $this->statementBuilder->getLoadUserListStatement();
        $this->storageService->executeStatement($statement);
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}