<?php


namespace Wmj\UserManager\Model\User;



use Wmj\UserManager\Model\EntryIndex;

/**
 * Class UserBuilder
 *
 * @package Wmj\UserManager\Model\User
 */
class UserBuilder
{
    /**
     * @var EntryIndex
     */
    protected UserIndex $userIndexProto;

    /**
     * UserBuilder constructor.
     *
     * @param UserIndex $userIndexProto
     */
    function __construct(UserIndex $userIndexProto){
        $this->userIndexProto = $userIndexProto;
    }

    function getIndexProto():UserIndex{
        return $this->userIndexProto;
    }

    /**
     * @param array $data
     *
     * @return User
     */
    function import(array $data):User{
        $user = new User ($this->userIndexProto->returnEntryIndex(($data[EntryIndex::INDEX_LABEL])));
        $user->import($data);
        return $user;
    }

    /**
     * @return User
     */
    function createUser():User{
        return new User($this->userIndexProto->cloneDefaultIndex());
    }

    function restoreUser(int $userIndex):User{
        return new User($this->userIndexProto->returnEntryIndex($userIndex));
    }

    /**
     * @param User $user
     * @param int  $newIndexValue
     */
    function changeIndexValue(User $user, int $newIndexValue):void{
        $user->setIndex($this->userIndexProto->returnEntryIndex($newIndexValue));
    }
}