<?php



namespace Wmj\UserManager\Model;


trait ReturnPendingAdditionsTrait
{
    /**
     * @param AbstractEntity ...$users
     *
     * @return array
     */
    protected function returnPendingAdditions(AbstractEntity ...$users): array
    {
        return array_merge(...array_map(fn(AbstractEntity $user) => $user->getPendingAdditions(), $users));
    }
}