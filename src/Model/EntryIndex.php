<?php


namespace Wmj\UserManager\Model;


use InvalidArgumentException;
use RuntimeException;
use Wmj\UserManager\ObjectCache\ObjectWeakCacheTrait;
use Wmj\UserManager\PrototypeInterface;
use Wmj\UserManager\PrototypeTrait;

abstract class EntryIndex implements PrototypeInterface//todo: make abstract and refactor usage to domain model specyfic index
{
    /**todo: index improvement
     *  * index will store visitor from repository allows check whether index value is in db
     *  * initialized object should one in entire app
     *  * after initialization clone should be forbidden | maybe use weak map | or throw exception on clone
     *  * where initialization should be done | in self | or in repository ?
     *  * if in self can set inex value only if id exist in db | use for check find id visitor
     *  * new row and id value will be generate during transaction in repository
     *  * index should store self instances for specyfic id and provide method to clone with id | if id in db return setted clone and save instance to static instance registry, if not throw exception
     *  * in that clone maybe use weak map as result ? | not becouse if delete row, entry will have null member
     *  * when delete row set index value to default | if value still exist in db throw exception
     *
     * create
     * set dependency
     * mark as read only | must be default
     * clone | must be read only
     * set | must exist in db
     *
     * whether can exist many instance of index with the same not default value ?
     */

    use ObjectWeakCacheTrait {
        addToCache as protected addToCacheBase;
        getFromCache as protected getFromCacheBase;
    }

    use PrototypeTrait {
        markAsPrototype as protected markAsPrototypeBase;
    }

    const ENTRY_CLASS = 'entry_class';
    const INDEX_LABEL = 'index';

    protected const DEFAULT_VALUE = null;
    protected ?int $indexValue = self::DEFAULT_VALUE;
    /**
     * @var IndexExistCheckVisitor
     */
    protected IndexExistCheckVisitor $indexChecker;

    /**
     * EntryIndex constructor.
     *
     * @param IndexExistCheckVisitor $indexExistCheckVisitor
     */
    function __construct(IndexExistCheckVisitor $indexExistCheckVisitor)
    {
        $this->indexChecker = $indexExistCheckVisitor;
    }

    /**
     * todo: test unit
     *  * set default value to value index
     *
     * @return $this
     */
    function reset(): self
    {
        $this->indexValue = self::DEFAULT_VALUE;

        return $this;
    }

    /**
     * todo: unit test
     *  * always return indexValue field
     *
     * @return int
     */
    function getIndexValue(): int
    {
        return $this->indexValue;
    }

    /**
     * @param int $index
     *
     * @return $this
     *
     * @throws ValidationException
     * @throws EntryNotExistInStorageException
     **/
    protected function setIndexValue(int $index): self
    {
        $this->indexValue = $index;
        $this->validateIndexValueOrFail();

        return $this;
    }

    /**
     * @return $this
     */
    function markAsPrototype(): self
    {
        if (!$this->isDefault()) throw new RuntimeException('Only default index can be prototype.');

        return $this->markAsPrototypeBase();
    }

    /**
     * todo:
     *  * if indexValue is equal to default value const retrun true
     *  * if indexValue is not equal to default value return false
     *
     * @return bool
     */
    function isDefault(): bool
    {
        return $this->indexValue === self::DEFAULT_VALUE;
    }

    /**
     * @param int $index
     *
     * @return $this
     * @throws ValidationException
     */
    function returnEntryIndex(int $index): self
    {
        if (!$this->isPrototype()) throw new RuntimeException('Only index prototype can return initialized index.');

        return $this->getFromCache($index) ?? $this->addToCache(
                (clone $this)->setIndexValue($index)
            );
    }

    /**
     * @param int $indexValue
     *
     * @return EntryIndex
     *
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    function getFromCache(int $indexValue): ?EntryIndex
    {
        return $this->getFromCacheBase($indexValue);
    }

    /**
     * @param EntryIndex $entryIndex
     *
     * @return EntryIndex
     */
    function addToCache(EntryIndex $entryIndex): EntryIndex
    {
        if ($entryIndex->isDefault()) throw new RuntimeException('Default index cannot be add to indexCache');
        $this->addToCacheBase($entryIndex);

        return $entryIndex;
    }

    /**
     * @return $this
     */
    function cloneDefaultIndex(): self
    {
        if (!$this->isPrototype()) throw new RuntimeException('Only index prototype can be cloned.');

        return clone $this;
    }

    /**
     * @return array
     * @throws RuntimeException
     */
    function export(): array
    {
        return [
            self::ENTRY_CLASS => $this->getEntryClass(),
            self::INDEX_LABEL => $this->indexValue
        ];
    }

    /**
     * Return entry class name to which index belongs.
     *
     * @return string
     */
    abstract function getEntryClass(): string;

    /**
     * @throws  EntryNotExistInStorageException
     * @throws ValidationException
     */
    protected function validateIndexValueOrFail(): void
    {
        switch (true) {
            case !$this->indexChecker->inStorage($this):
                throw new EntryNotExistInStorageException($this->getEntryClass(), $this->indexValue);

            case $this->indexValue <= 0:
                throw new ValidationException("EntryIndex cannot be smaller than 1.");
        }
    }
}