<?php



namespace Wmj\UserManager\Model;


trait MySqlStatementStringGeneratorTrait
{
    /**
     * @param array $mutations
     *
     * @return string
     */
    function convertToUpdateString(array $mutations): string
    {
        return implode(',', $this->mergeOffsetWithValues($mutations));
    }

    /**
     * @param array $offsets
     * @param array $values
     *
     * @return array
     */
    protected function mergeOffsetWithValues(array $mutations): array
    {
        $result = [];
        $backQuote = '`';
        $quote = "'";
        // todo: escape all quotes inside string or use PDO prepare
        foreach ($mutations as $offset => $value){
            $offset = ltrim(rtrim($offset, $backQuote) , $backQuote);

            if(is_string($value)) {
                $value = ltrim(rtrim($value, $quote) , $quote);
                $value = $this->quoteString($value);
            }

            array_push($result, "{$this->backQuoteString($offset)}=$value");
        }

        return $result;
    }


    /**
     * @param array $values
     *
     * @return array
     */
    protected function surroundStringByBackQuote(array $values): array
    {
        return array_map(fn($value) => is_string($value) ? $this->backQuoteString($value) : $value, $values);
    }

    protected function backQuoteString(string $value):string{
        return "`$value`";
    }

    protected function quoteString(string $value):string{
        return "'$value'";
    }

    /**
     * @param array $values
     *
     * @return array
     */
    protected function surroundStringByQuote(array $values): array
    {
        return array_map(fn($value) => is_string($value) ? $this->quoteString($value) : $value, $values);
    }
}