<?php


namespace Wmj\UserManager\Model;

use BadMethodCallException;

/**
 * Class IndexExistCheckVisitor
 *
 * @package Wmj\UserManager\Model
 */
class IndexExistCheckVisitor
{
    /**
     * @var callable
     */
    protected $indexChecker;

    /**
     * @param callable $indexChecker
     */
    function setIndexCheckerCallback(callable $indexChecker){
        $this->indexChecker = $indexChecker;
    }

    /**
     * @param EntryIndex $index
     *
     * @return bool
     */
    function inStorage(EntryIndex $index): bool
    {
        if(!is_callable($this->indexChecker)) throw new BadMethodCallException('Index checker callback isn\'t set.');

        return (bool)call_user_func($this->indexChecker, $index);
    }
}