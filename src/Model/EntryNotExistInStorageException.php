<?php


namespace Wmj\UserManager\Model;






class EntryNotExistInStorageException extends \DomainException
{
    function __construct(string $entryName, int $indexValue)
    {
        parent::__construct("$entryName:$indexValue found in storage.");
    }
}