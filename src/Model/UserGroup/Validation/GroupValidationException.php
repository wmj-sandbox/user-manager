<?php


namespace Wmj\UserManager\Model\UserGroup\Validation;


use Wmj\UserManager\Model\ValidationException;

/**
 * Class GroupValidationException
 *
 * @package Wmj\UserManager\Model\UserGroup\Validation
 */
class GroupValidationException extends ValidationException
{

}