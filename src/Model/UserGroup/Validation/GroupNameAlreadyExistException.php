<?php


namespace Wmj\UserManager\Model\UserGroup\Validation;

/**
 * Class GroupNameAlreadyExistException
 *
 * @package Wmj\UserManager\Model\UserGroup\Validation
 */
class GroupNameAlreadyExistException extends GroupValidationException
{

}