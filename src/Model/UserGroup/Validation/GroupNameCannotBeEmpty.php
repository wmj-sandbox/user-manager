<?php


namespace Wmj\UserManager\Model\UserGroup\Validation;


/**
 * Class GroupNameCannotBeEmpty
 *
 * @package Wmj\UserManager\Model\UserGroup\Validation
 */
class GroupNameCannotBeEmpty extends GroupValidationException
{

}