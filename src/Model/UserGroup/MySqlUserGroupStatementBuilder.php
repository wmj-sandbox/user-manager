<?php


namespace Wmj\UserManager\Model\UserGroup;


use PDO;
use PDOStatement;
use Wmj\UserManager\Model\EntryIndex;


/**
 * Class MySqlUserGroupStatementBuilder
 *
 * @package Wmj\UserManager\Model\UserGroup
 */
class MySqlUserGroupStatementBuilder
{
    const TABLE_NAME = UserGroup::COLLECTION_NAME;
    const INDEX = EntryIndex::INDEX_LABEL;
    const COLUMN_NAME = UserGroup::NAME;

    const MEMBERS_TABLE_NAME = 'group_members';
    const MEMBERS_GROUP_INDEX = 'group_index';
    const MEMBERS_USER_INDEX = 'user_index';

    private PDO $pdo;

    /**
     * MySqlUserGroupStatementBuilder constructor.
     *
     * @param PDO $PDO
     */
    function __construct(PDO $PDO)
    {
        $this->pdo = $PDO;
    }

    /**
     * @return PDOStatement
     */
    function returnGroupCreateStatement(): PDOStatement
    {
        $tableName = self::TABLE_NAME;
        $index = self::INDEX;
        $name = self::COLUMN_NAME;
        $membersTableName = self::MEMBERS_TABLE_NAME;
        $membersGroupIndex = self::MEMBERS_GROUP_INDEX;
        $membersUserIndex = self::MEMBERS_USER_INDEX;

        return $this->pdo->prepare(<<<END
            CREATE TABLE IF NOT EXISTS `$tableName` (
                `$index` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
                `$name` varchar(30) NULL UNIQUE,
                PRIMARY KEY (`$index`)
            )DEFAULT CHARSET=utf8;
            
            CREATE TABLE IF NOT EXISTS `$membersTableName`(
                `$membersGroupIndex` BIGINT UNSIGNED NOT NULL,
                `$membersUserIndex` BIGINT UNSIGNED NOT NULL,
                INDEX member_ind ($membersUserIndex),
                FOREIGN KEY ($membersUserIndex) 
                REFERENCES users(`$index`) 
                ON UPDATE CASCADE ON DELETE CASCADE,
                INDEX group_ind ($membersGroupIndex),
                FOREIGN KEY ($membersGroupIndex) 
                REFERENCES $tableName(`$index`) 
                ON UPDATE CASCADE ON DELETE CASCADE
            )DEFAULT CHARSET=utf8;
        END
        );
    }

    /**
     * @return PDOStatement
     */
    function getAddGroupStatement(): PDOStatement
    {
        $tableName = self::TABLE_NAME;


        return $this->pdo->prepare(<<<END
            INSERT INTO `$tableName` () VALUES ();
        END
        );

    }

//    /**
//     * @param UserGroup $userGroup
//     *
//     * @return PDOStatement
//     */
//    function getDeleteGroupStatement(UserGroup $userGroup): PDOStatement
//    {
//        if ($userGroup->getIndexProto()->isDefault()) throw new RuntimeException('Cannot prepare delete statement for not saved group.');
//
//        $tableName = self::TABLE_NAME;
//        $index = self::INDEX;
//
//        return $this->pdo->prepare(<<<END
//            DELETE FROM `$tableName` WHERE `$index`={$userGroup->getIndexProto()->getIndexValue()};
//        END
//        );
//    }

    /**
     * Removes records wit provided indexes.
     * @param array $indexLIst
     *
     * @return PDOStatement
     */
    function deleteStatement(array $indexLIst): PDOStatement{

        $tableName = $this::TABLE_NAME;
        $index = EntryIndex::INDEX_LABEL;
        $inddeString = implode(',', $indexLIst);

        return $this->pdo->prepare(<<<END
            DELETE FROM `{$tableName}` WHERE `{$index}` IN ({$inddeString});
        END);
    }

    /**
     * @param UserGroup $userGroup
     * @param string    $nameLabel
     *
     * @return PDOStatement
     */
    function getUpdatGroupStatement(UserGroup $userGroup, string $nameLabel): PDOStatement
    {
        $tableName = self::TABLE_NAME;
        $index = self::INDEX;
        $name = self::COLUMN_NAME;

        return $this->pdo->prepare(<<<END
            UPDATE `$tableName` SET `$name`=$nameLabel WHERE `$index`={$userGroup->getIndex()->getIndexValue()};
        END
        );
    }

    /**
     * @param UserGroup $group
     * @param string    $memberIndexLabel
     *
     * @return PDOStatement
     */
    function getAddGroupMembersStatement(UserGroup $group, string $memberIndexLabel): PDOStatement
    {
        $membersTableName = self::MEMBERS_TABLE_NAME;
        $membersUserIndex = self::MEMBERS_USER_INDEX;
        $membersGroupIndex = self::MEMBERS_GROUP_INDEX;

        return $this->pdo->prepare(<<<END
            INSERT IGNORE INTO `$membersTableName` ($membersGroupIndex, $membersUserIndex) VALUES ({$group->getIndex()->getIndexValue()}, $memberIndexLabel);
        END
        );
    }

    /**
     * @param UserGroup $group
     * @param string    $memberIndexLabel
     *
     * @return PDOStatement
     */
    function getRemoveGroupMemberStatement(UserGroup $group, string $memberIndexLabel): PDOStatement
    {
        $membersTableName = self::MEMBERS_TABLE_NAME;
        $membersUserIndex = self::MEMBERS_USER_INDEX;
        $membersGroupIndex = self::MEMBERS_GROUP_INDEX;

        return $this->pdo->prepare(<<<END
            DELETE FROM `$membersTableName` WHERE `$membersUserIndex`=$memberIndexLabel AND `$membersGroupIndex`={$group->getIndex()->getIndexValue()};
        END
        );
    }

    /**
     * @return PDOStatement
     */
    function getSelectAllGroupStatement(): PDOStatement{
        $tableName = self::TABLE_NAME;
        $index = self::INDEX;
        $name = self::COLUMN_NAME;


        return $this->pdo->prepare(<<<END
            SELECT `$index`,`$name` FROM `$tableName` WHERE `$name` != '' ORDER BY `$index` DESC;
        END);
    }

    /**
     * @param EntryIndex $groupIndex
     *
     * @return PDOStatement
     */
    function getSelectGroupDetailsStatement(EntryIndex $groupIndex): PDOStatement{
        $tableName = self::TABLE_NAME;
        $index = self::INDEX;
        return $this->pdo->prepare(<<<END
            SELECT * FROM `$tableName` WHERE `$index`={$groupIndex->getIndexValue()};
        END);
    }

    /**
     * @param EntryIndex $groupIndex
     *
     * @return PDOStatement
     */
    function getSelectGroupMembersStatement(EntryIndex $groupIndex):PDOStatement{
        $membersTableName = self::MEMBERS_TABLE_NAME;
        $membersGroupIndex = self::MEMBERS_GROUP_INDEX;
        $membersUserIndex = self::MEMBERS_USER_INDEX;


        return $this->pdo->prepare(<<<END
            SELECT `$membersUserIndex` FROM `$membersTableName` WHERE `$membersGroupIndex` = {$groupIndex->getIndexValue()};
        END);
    }

    /**
     * @param EntryIndex $userIndex
     *
     * @return PDOStatement
     */
    function getSelectUserGroupsStatement(EntryIndex $userIndex): PDOStatement{
        $membersTableName = self::MEMBERS_TABLE_NAME;
        $membersGroupIndex = self::MEMBERS_GROUP_INDEX;
        $membersUserIndex = self::MEMBERS_USER_INDEX;

        return $this->pdo->prepare(<<<END
            SELECT `$membersGroupIndex` FROM `$membersTableName` WHERE `$membersUserIndex` = {$userIndex->getIndexValue()};
        END);
    }

    function entryExistStmt(EntryIndex $index): PDOStatement{
        $tableName = $this::TABLE_NAME;
        $indexColumn = $this::INDEX;

        return $this->pdo->prepare(<<<END
            SELECT COUNT(DISTINCT `$indexColumn`) FROM `$tableName` WHERE `$indexColumn` = {$index->getIndexValue()}; 
        END);
    }
}