<?php


namespace Wmj\UserManager\Model\UserGroup;


use PDO;
use Wmj\UserManager\Model\IndexExistCheckVisitor;
use Wmj\UserManager\Model\StorageService;
use Wmj\UserManager\ObjectCache\ObjectWeakCacheTrait;

/**
 * Class UserGroupFactory
 *
 * @package Wmj\UserManager\Model\UserGroup
 */
class UserGroupFactory
{
    use ObjectWeakCacheTrait;

    protected PDO $pdo;
    /**
     * @var StorageService
     */
    protected StorageService $storageService;

    /**
     * UserGroupFactory constructor.
     *
     * @param PDO            $PDO
     * @param StorageService $storageService
     */
    function __construct(PDO $PDO, StorageService $storageService)
    {
        $this->pdo = $PDO;
        $this->storageService = $storageService;

        $repo = $this->getGroupRepository();
        $this->getIndexChecker()->setIndexCheckerCallback(fn(UserGroupIndex $index): bool => $repo->entryExist($index));
    }

    /**
     * @return UserGroupReposiotry
     *
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    function getGroupRepository(): UserGroupReposiotry
    {
        return $this->getFromCache(UserGroupReposiotry::class) ?? $this->addToCache(
                new UserGroupReposiotry($this->storageService, $this->getMySqlUserStatementCreator(), $this->makeGroupBuilder())
            );
    }


    /**
     * @return MySqlUserGroupStatementBuilder
     *
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    protected function getMySqlUserStatementCreator(): MySqlUserGroupStatementBuilder
    {
        return $this->getFromCache(MySqlUserGroupStatementBuilder::class) ?? $this->addToCache(
                new MySqlUserGroupStatementBuilder($this->pdo)
            );
    }

    /**
     * @return UserGroupBuilder
     */
    function makeGroupBuilder(): UserGroupBuilder
    {
        return new UserGroupBuilder($this->getIndexProto());
    }

    /**
     * @return UserGroupIndex
     *
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    protected function getIndexProto(): UserGroupIndex
    {
        return $this->getFromCache(UserGroupIndex::class) ?? $this->addToCache(
                (new UserGroupIndex($this->getIndexChecker()))->markAsPrototype()
            );
    }

    /**
     * @return IndexExistCheckVisitor
     *
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    protected function getIndexChecker(): IndexExistCheckVisitor
    {
        return $this->getFromCache(IndexExistCheckVisitor::class) ?? $this->addToCache(
                new IndexExistCheckVisitor()
            );
    }
}