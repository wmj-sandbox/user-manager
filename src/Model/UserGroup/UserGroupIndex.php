<?php


namespace Wmj\UserManager\Model\UserGroup;


use Wmj\UserManager\Model\EntryIndex;

/**
 * Class UserGroupIndex
 *
 * @package Wmj\UserManager\Model\UserGroup
 */
class UserGroupIndex extends EntryIndex
{
    /**
     * @return string
     */
    function getEntryClass(): string
    {
        return UserGroup::class;
    }
}