<?php


namespace Wmj\UserManager\Model;


use ArrayAccess;

abstract class AbstractEntity implements ArrayAccess, CommitMutationInterface
{
    use CommitMutationTratit;

    function import(array $data): void
    {
        $this->filterNotAllowedOffset($data);
        $this->filterNullValues($data);
        $this->members = $data;
    }

    private function filterNotAllowedOffset(array &$data): void
    {
        $data = array_intersect_key($data, $this->getSimpleValueNameProcessorPairs());
    }

    /**
     * Returning array of simple member value, offset as key and processor as value. Processor get value as argument, validate and sanitize and return.
     * @return array<string, callable>
     */
    abstract protected function getSimpleValueNameProcessorPairs(): array;

    private function filterNullValues(array &$data): void
    {
        $data = array_filter($data, fn($value) => !is_null($value));
    }

    function getAll(): array
    {
        return [...$this->getComplexValues(), ...$this->members];
    }

    /**
     * @param array $values
     *
     * @return $this
     *
     * @throws ValidationException
     */
    function set(array $values):self{
        array_walk($values, fn($value, $offset) => $this->offsetSet($offset, $value));
        return $this;
    }

    abstract protected function getComplexValues(): array;

    function offsetGet($offset)
    {
        return $this->members[$offset] ?? null;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     *
     * @throws ValidationException
     */
    function offsetSet($offset, $value): void
    {
        if ($this->offsetExists($offset)) $this->scheduleToAdd($this->processMemberValue($offset, $value), $offset);
    }

    function offsetExists($offset): bool
    {
        return array_key_exists($offset, $this->getSimpleValueNameProcessorPairs());
    }

    private function processMemberValue($offset, $value)
    {
        return $this->getSimpleValueNameProcessorPairs()[$offset]($value);
    }

    function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) $this->scheduleToRemove($offset);
    }
}