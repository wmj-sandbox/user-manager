<?php


namespace Wmj\UserManager;


use Psr\Container\ContainerExceptionInterface;
use RuntimeException;

/**
 * Class DependencyNotFoundException
 *
 * @package Wmj\UserManager
 */
class DependencyNotFoundException extends RuntimeException implements ContainerExceptionInterface
{

}