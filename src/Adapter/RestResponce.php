<?php


namespace Wmj\UserManager\Adapter;


class RestResponce implements RestApiInterface
{
    private string $json_data = '{}';
    private int $code = self::NOT_FOUND;

    function setCode(int $code):self{
        $this->code = $code;
        return $this;
    }

    function setContent(array $data):self{
        $this->json_data = json_encode($data);
    }

    function send():void{
        http_response_code($this->code);
        header('Content-Type: application/json; charset=utf-8');
        echo $this->json_data;
    }
}