<?php


namespace Wmj\UserManager\Adapter;


class DefaultEndpoint implements RestApiInterface, EndpointInterface
{
    //todo: validate api key

    function handleRequest(string $method, string $uri, array $inputData): void
    {
        $this->sendResponceCode(self::METHOD_NOT_ALLOWED);
    }


    protected function sendResponceCode(int $code): void
    {
        http_response_code($code);
    }

    protected function sendJsonResponce(int $code, string $jsonPayload):void{
        header('Content-Type: application/json; charset=utf-8');
        $this->sendResponceCode($code);
        echo $jsonPayload;
    }

    protected function getLastUriItem($uri){
        $uri = explode('/', $uri);
        return end ($uri);
    }

    protected function getOneFeforeLastUriItem(string $uri){
        $uri = explode('/', $uri);
        return $uri[count($uri) - 2];
    }
}