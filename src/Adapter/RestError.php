<?php


namespace Wmj\UserManager\Adapter;


class RestError implements RestApiInterface
{
    function sendAndExit(int $code, string $errorName, string $errorMessage, string $method, string $endpointPath): void
    {
        http_response_code($code);
        header(self::APPLICATION_JSON_TYPE);

        //todo: add validtion error as array
        // failedValidations:{
        //  inputPropertyName: [
        //          messageFromException1,
//                  'messageFromException2',
//                  'etc'
        //      ]}

        echo json_encode([
            "timestamp" => gmdate("Y-m-d\TH:i:s\Z"),
            "error" => $errorName,
            "message" => $errorMessage,
            "method" => $method,
            "path" => $endpointPath
        ]);

        exit();
    }
}