<?php


namespace Wmj\UserManager\Adapter\RestApi\UserGroups;


use Exception;
use RuntimeException;
use Wmj\UserManager\Adapter\DefaultEndpoint;
use Wmj\UserManager\Model\EntryIndex;
use Wmj\UserManager\Model\UserGroup\UserGroup;
use Wmj\UserManager\Model\UserGroup\UserGroupBuilder;
use Wmj\UserManager\Model\UserGroup\UserGroupReposiotry;
use Wmj\UserManager\Model\ValidationException;

/**
 * Class GroupCollectionEndpoint
 *
 * @package Wmj\UserManager\Adapter\RestApi\UserGroups
 */
class GroupCollectionEndpoint extends DefaultEndpoint
{
    /**
     * @var UserGroupReposiotry
     */
    private UserGroupReposiotry $groupRepository;
    /**
     * @var UserGroupBuilder
     */
    private UserGroupBuilder $groupBuilder;

    /**
     * GroupCollectionEndpoint constructor.
     *
     * @param UserGroupReposiotry $groupRepository
     * @param UserGroupBuilder    $groupBuilder
     */
    function __construct(UserGroupReposiotry $groupRepository, UserGroupBuilder $groupBuilder)
    {
        $this->groupRepository = $groupRepository;
        $this->groupBuilder = $groupBuilder;
    }

    /**
     * @inheritDoc
     */
    public function handleRequest(string $method, string $uri, array $inputData): void
    {

        switch ($method) {
            case (self::GET):
                $this->sendGroupList();
                break;
            case (self::POST):
                $this->createGroup($inputData);
                break;
            default:
                parent::handleRequest($method, $uri, $inputData);
        }
    }

    /**
     *
     */
    protected function sendGroupList(): void
    {
        try {
            $this->sendJsonResponce(self::OK, json_encode($this->groupRepository->getGroupList()));
        } catch (Exception $exception) {
            $this->sendResponceCode(self::INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param array $payload
     *
     * @throws RuntimeException
     */
    protected function createGroup(array $payload): void
    {
        try {
            $group = $this->groupBuilder->createUserGroup()->set($payload);
            $this->groupRepository->save($group);

            $this->sendJsonResponce(self::CREATED, json_encode([
                EntryIndex::INDEX_LABEL => $group->getIndex()->getIndexValue(),
                UserGroup::NAME => $group->offsetGet(UserGroup::NAME)
            ]));
        } catch (ValidationException $exception) {
            $this->sendResponceCode(self::BAD_REQUEST);
        } catch (Exception $exception) {
            $this->sendResponceCode(self::INTERNAL_SERVER_ERROR);
            throw new RuntimeException($exception->getMessage());
        }
    }
}