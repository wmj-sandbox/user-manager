<?php


namespace Wmj\UserManager\Adapter\RestApi\Users;


use Exception;
use RuntimeException;
use Wmj\UserManager\Adapter\DefaultEndpoint;
use Wmj\UserManager\Model\EntryIndex;
use Wmj\UserManager\Model\User\UserBuilder;
use Wmj\UserManager\Model\UserGroup\UserGroupReposiotry;

/**
 * Class UserGroupEndpoint
 *
 * @package Wmj\UserManager\Adapter\RestApi\Users
 */
class UserGroupEndpoint extends DefaultEndpoint
{
    /**
     * @var UserGroupReposiotry
     */
    protected UserGroupReposiotry $groupRepo;
    /**
     * @var EntryIndex
     */
    protected EntryIndex $indexProto;
    /**
     * @var UserBuilder
     */
    protected UserBuilder $userBuilder;

    /**
     * UserGroupEndpoint constructor.
     *
     * @param UserGroupReposiotry $userGroupRepository
     * @param UserBuilder         $userBuilder
     */
    function __construct(UserGroupReposiotry $userGroupRepository, UserBuilder $userBuilder)
    {
        $this->groupRepo = $userGroupRepository;
        $this->userBuilder = $userBuilder;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array  $inputData
     */
    function handleRequest(string $method, string $uri, array $inputData): void
    {
        switch ($method) {
            case self::GET:
                $this->sendUserGroups($this->getOneFeforeLastUriItem($uri));
                break;
            default:
                parent::handleRequest($method, $uri, $inputData);
        }
    }

    /**
     * @param int $userId
     */
    protected function sendUserGroups(int $userId): void
    {
        try {
            $this->sendJsonResponce(self::OK, json_encode(
                $this->groupRepo->getGroupsWithUser($this->userBuilder->getIndexProto()->returnEntryIndex($userId))));
        } catch (Exception $exception) {
            $this->sendResponceCode(self::INTERNAL_SERVER_ERROR);
            throw new RuntimeException($exception);
        }
    }
}