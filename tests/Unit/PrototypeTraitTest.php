<?php

namespace Wmj\UserManager\Tests\Unit;

use PHPUnit\Framework\MockObject\MockObject;
use Throwable;
use Wmj\UserManager\PrototypeTrait;
use PHPUnit\Framework\TestCase;

class PrototypeTraitTest extends TestCase
{
    /**
     * @var MockObject|PrototypeTrait
     */
    protected $proto;

    function setUp(): void
    {
        $this->proto = $this->getMockForTrait(PrototypeTrait::class);
    }

    function test_at_default_prototype_is_editable(): void
    {
        $this->assertFalse($this->proto->isPrototype());
    }

    function test_prototype_inform_whether_is_read_only(): void
    {
        $this->assertFalse($this->proto->isPrototype());
        $this->proto->markAsPrototype();
        $this->assertTrue($this->proto->isPrototype());
    }

    function test_default_clone_method_is_not_accessible(): void
    {
        $this->expectException(Throwable::class);
        $cl = clone $this->proto;
    }

    function test_clone_of_prototype_is_not_mark_as_prototype(): void
    {
        $proto = new class {
            use PrototypeTrait;

            function testClone(): self
            {
                return clone $this;
            }
        };

        $proto->markAsPrototype();
        $clone = $proto->testClone();

        $this->assertFalse($clone->isPrototype());
    }
}
