FROM php:7.4-apache
RUN docker-php-ext-install mysqli pdo pdo_mysql
#RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
#RUN sudo apt-get install php-mysql
RUN apt-get update && apt-get upgrade -y
RUN a2enmod rewrite
RUN service apache2 restart